package cz.ox.tests.template;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.logging.Handler;

/**
 * Created by Crusty on 7.5.2015.
 */
public class NovaTrida {
    int param = 0;

    public void work(Context context, View rootView) {
        Log.d("?", "work ...");

        // ukaz Toast
        Toast.makeText(context, "- BAF -", Toast.LENGTH_SHORT).show();

        rootView.postDelayed(new Runnable() {
            @Override
            public void run() {
                sleep(3000);
            }
        }, 1000);

        Log.d("?", "... done");
    }

    private void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
