package cz.ox.tests.test4;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;

import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import cz.ox.tests.R;
import cz.ox.tests.template.NovaTrida;

/**
 * Created by Crusty on 25.9.2014.
 */
public class TextFontsAndAnimationsActivity extends Activity {

    RelativeLayout barrelLayout;
    ProgressBar barrel;
    Display display;
    Point screenSize;
    Timer timer;
    TimerTask timerTask;
    TextView text;

    int screenWidth;
    int screenHeight;
    int barrelWidth;
    int barrelHeight;
    int oilAmount;

    int up = 0;
    int down = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test4);
        final View rootView = findViewById(R.id.root);

        Button b;
        TextView tv;

        View.OnClickListener onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation oldAnim = v.getAnimation();
                if(oldAnim != null) {
                    oldAnim.reset();
                    oldAnim.cancel();
                    v.setAnimation(null);
                    return;
                }

                ScaleAnimation anim = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.0f,
                        ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                        ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                anim.setDuration(1000);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                v.startAnimation(anim);
            }
        };

        tv = (TextView) findViewById(R.id.text1);
        tv.setOnClickListener(onClick);
        tv = (TextView) findViewById(R.id.text2);
        tv.setOnClickListener(onClick);
        tv = (TextView) findViewById(R.id.text3);
        tv.setOnClickListener(onClick);
        tv = (TextView) findViewById(R.id.text4);
        tv.setOnClickListener(onClick);

        NovaTrida nt = new NovaTrida();
        nt.work(this, rootView);


        // misebo AF
        text = (TextView) findViewById(R.id.text1);

        //barrelLayout = (RelativeLayout) findViewById(R.id.BarrelLayout);
        //barrelLayout.getLayoutParams().width = barrelWidth;
        //barrelLayout.getLayoutParams().height = barrelHeight;

        //barrel = (ProgressBar) findViewById(R.id.Barrel);

        display = getWindowManager().getDefaultDisplay();
        screenSize = new Point();
        display.getSize(screenSize);
        screenWidth = screenSize.x;
        screenHeight = screenSize.y;
        barrelWidth = screenWidth / 2;
        barrelHeight = screenHeight / 5;

        oilAmount = 0; // maximum je 15898 (158,98 litru (jeden barel) × 100 pro presnost)

        //barrel.setProgress(oilAmount);
    }

    @Override
    public void onPause(){
        super.onPause();
        timer.cancel();
    }

    @Override
    public void onResume(){
        super.onResume();
        try {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if(oilAmount >= 15898) {
                        up = 0;
                        down = 1;
                    }

                    if(oilAmount <= 0) {
                        up = 1;
                        down = 0;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(up == 1) {
                                oilAmount = oilAmount + 1;
                                barrel.setProgress(oilAmount);
                                text.setText("Napousteni");
                            }
                            if(down == 1) {
                                oilAmount = oilAmount - 1;
                                barrel.setProgress(oilAmount);
                                text.setText("Vypousteni");
                            }
                        }
                    });
                }
            };
            timer.schedule(timerTask, 0, 1);
        } catch (IllegalStateException e){
            android.util.Log.i("Damn", "resume error");
            e.printStackTrace();
        }

        try {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(up == 1) {
                                text.setText("Napousteni");
                            }
                            if(down == 1) {
                                text.setText("Vypousteni");
                            }
                        }
                    });

                }
            };
            timer.schedule(timerTask, 0, 100); // Tady se nastavuje cas v ms, po kterem se to opakuje
        } catch (IllegalStateException e){
            android.util.Log.i("Damn", "resume error");
        }
    }
}
