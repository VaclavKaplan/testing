package cz.ox.tests.testViewAnims;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import cz.ox.tests.R;

/**
 */
public class ViewAnimsActivity extends Activity {

    private static final String TAG = ViewAnimsActivity.class.getName();

    ArrayList<View> mViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_anims);

        Button b;
        ImageView iv;
        TextView tv;
        Animation anim;

        b = (Button) findViewById(R.id.reset);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation a;
                for(View vv : mViews) {
                    a = vv.getAnimation();
                    if(a == null)
                        continue;

                    Log.d(TAG, "anim "+a);
                    a.cancel();
                    a.reset();
                    a.startNow();
                }
            }
        });

        iv = (ImageView) findViewById(R.id.image);
        anim = AnimationUtils.loadAnimation(this, R.anim.blink);
        anim.setDuration(2000);
        iv.setAnimation(anim);
        mViews.add(iv);

        tv = (TextView) findViewById(R.id.text);
        anim = AnimationUtils.loadAnimation(this, R.anim.together2);
        anim.setDuration(2000);
        tv.setAnimation(anim);
        mViews.add(tv);
    }
}
