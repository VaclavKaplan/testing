package cz.ox.tests.testListView;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.ox.tests.R;

public class testListViewActivity extends ActionBarActivity {

    String TAG = "listView";

    SparseArray<Group> groups = new SparseArray<Group>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_list_view);

        createData();
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
        MyExpandableListAdapter adapter = new MyExpandableListAdapter(this,
                groups);
        listView.setAdapter(adapter);
        listView.setItemsCanFocus(true);

        /*MyAdapter adapter = new MyAdapter(this, 0);
        for(int x = 0; x < 10; x++) {
            adapter.add(new ListItem("item " + x));
        }
        final ListView lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);
        lv.setItemsCanFocus(true);
        lv.setClickable(false);*/
        /*lv.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                Log.d(TAG, "onLayoutChange");
            }
        });*/

        /*lv.getViewTreeObserver().addOnGlobalFocusChangeListener(new ViewTreeObserver.OnGlobalFocusChangeListener() {
            @Override
            public void onGlobalFocusChanged(View oldFocus, View newFocus) {
                Log.d(TAG, "old "+oldFocus+", new "+newFocus);
            }
        });

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.d(TAG, "globalLayout");

                View v = lv.getChildAt(3);
                v.requestFocus();
            }
        });*/
    }

    public void createData() {
        for (int j = 0; j < 5; j++) {
            Group group = new Group("Test " + j);
            for (int i = 0; i < 5; i++) {
                group.children.add(new ListItem("Sub Item" + i, i % 2 == 0));
            }
            groups.append(j, group);
        }
    }

    class Group {

        public String string;
        public final List<ListItem> children = new ArrayList<ListItem>();

        public Group(String string) {
            this.string = string;
        }
    }

    class ListItem {
        String mText;
        boolean mBoolean;

        public ListItem(String s, boolean b) {
            mText = s;
            mBoolean = b;
        }
    }

    public class MyExpandableListAdapter extends BaseExpandableListAdapter {

        private final SparseArray<Group> groups;
        public LayoutInflater inflater;
        public Activity activity;

        public MyExpandableListAdapter(Activity act, SparseArray<Group> groups) {
            activity = act;
            this.groups = groups;
            inflater = act.getLayoutInflater();
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return groups.get(groupPosition).children.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            final ListItem children = (ListItem) getChild(groupPosition, childPosition);
            EditText text = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item, null);
            }
            text = (EditText) convertView.findViewById(R.id.edit);
            text.setText(children.mText);
            text.setInputType(children.mBoolean?InputType.TYPE_CLASS_NUMBER:InputType.TYPE_CLASS_TEXT);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(activity, children.mText,
                            Toast.LENGTH_SHORT).show();
                }
            });
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return groups.get(groupPosition).children.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groups.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groups.size();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            super.onGroupCollapsed(groupPosition);
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.list_item_group, null);
            }
            Group group = (Group) getGroup(groupPosition);
            ((CheckedTextView) convertView).setText(group.string);
            ((CheckedTextView) convertView).setChecked(isExpanded);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    private class MyAdapter extends ArrayAdapter<ListItem> {

        public MyAdapter(Context context, int resource) {
            super(context, resource);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.list_item, null);
            }

            ListItem li = getItem(position);

            EditText et = (EditText) convertView.findViewById(R.id.edit);
            et.setText(li.mText);

            return convertView;
        }
    }
}
