package cz.ox.tests.FloodFill;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.Queue;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class FloodFillActivity extends Activity {

    ImageView mImageView;
    ImageView mImageView2;
    Canvas mCanvas;
    Bitmap mPictureBitmap, mBitmap, mSceneBitmap, mBlurBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_flood_fill);

        mImageView = (ImageView) findViewById(R.id.drawContainer);
        mImageView2 = (ImageView) findViewById(R.id.drawContainer2);

        mImageView.post(new Runnable() {
            @Override
            public void run() {
                setCanvas();

                //int targetColor = mSceneBitmap.getPixel((int) event.getX(), (int) event.getY());
                int targetColor = 0xff000000;
                FloodFill fill = new FloodFill(mBitmap, targetColor, Color.argb(255, 255, 0, 0));
                //fill.floodFill((int) event.getX(), (int) event.getY());
                fill.floodFill((int) 120, (int) 100);
                Bitmap bmp = fill.getImage();
                mCanvas.drawBitmap(bmp, 0, 0, null);
                mImageView.invalidate();
            }
        });
    }

    public void setCanvas() {
        {
            mPictureBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
            mBitmap = Bitmap.createScaledBitmap(mPictureBitmap, mImageView.getWidth(), mImageView.getHeight(), false);
            mPictureBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
            mBitmap = mPictureBitmap.copy(Bitmap.Config.ARGB_8888, true);
            mSceneBitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, true);
            //mBlurBitmap = blurImage(mPictureBitmap);
            mCanvas = new Canvas(mBitmap);
            mImageView.setImageBitmap(mBitmap);
            mImageView2.setImageBitmap(mPictureBitmap);
            //mBlur.setImageBitmap(mBlurBitmap);

            // failure to set these layer types correctly will result in a black canvas after drawing.
            mImageView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mImageView2.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            mImageView.bringToFront();
            //mAllowedToDraw = true;

            //setImageViewOnTouch();
        }
    }

    public class FloodFill {
        protected Bitmap mImage = null;
        protected int[] mTolerance = new int[] { 50, 50, 50, 50 };
        protected int mWidth = 0;
        protected int mHeight = 0;
        protected int[] mPixels = null;
        protected int mFillColor = 0;
        protected int[] mStartColor = new int[] { 0, 0, 0, 0 };
        protected boolean[] mPixelsChecked;
        protected Queue<FloodFillRange> mRanges;

        public FloodFill(Bitmap img) {
            copyImage(img);
        }

        public FloodFill(Bitmap img, int targetColor, int newColor) {
            useImage(img);

            setFillColor(newColor);
            setTargetColor(targetColor);
        }

        public void setTargetColor(int targetColor) {
            mStartColor[0] = Color.red(targetColor);
            Log.v("Red", "" + mStartColor[0]);
            mStartColor[1] = Color.green(targetColor);
            Log.v("Green", "" + mStartColor[1]);
            mStartColor[2] = Color.blue(targetColor);
            Log.v("Blue", "" + mStartColor[2]);
            mStartColor[3] = Color.alpha(targetColor);
            Log.v("Alpha", "" + mStartColor[3]);
        }

        public int getFillColor() {
            return mFillColor;
        }

        public void setFillColor(int value) {
            mFillColor = value;
        }

        public int[] getTolerance() {
            return mTolerance;
        }

        public void setTolerance(int[] value) {
            mTolerance = value;
        }

        public void setTolerance(int value) {
            mTolerance = new int[] { value, value, value, value };
        }

        public Bitmap getImage() {
            return mImage;
        }

        public void copyImage(Bitmap img) {
            mWidth = img.getWidth();
            mHeight = img.getHeight();

            mImage = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mImage);
            canvas.drawBitmap(img, 0, 0, null);

            mPixels = new int[mWidth * mHeight];

            mImage.getPixels(mPixels, 0, mWidth, 0, 0, mWidth, mHeight);
        }

        public void useImage(Bitmap img) {
            mWidth = img.getWidth();
            mHeight = img.getHeight();
            mImage = img;

            mPixels = new int[mWidth * mHeight];

            mImage.getPixels(mPixels, 0, mWidth, 0, 0, mWidth, mHeight);
        }

        protected void prepare() {
            mPixelsChecked = new boolean[mPixels.length];
            mRanges = new LinkedList<FloodFillRange>();
        }

        public void floodFill(int x, int y) {
            // Setup
            prepare();

            if (mStartColor[0] == 0) {
                // ***Get starting color.
                int startPixel = mPixels[(mWidth * y) + x];
                mStartColor[0] = (startPixel >> 16) & 0xff;
                mStartColor[1] = (startPixel >> 8) & 0xff;
                mStartColor[2] = startPixel & 0xff;
            }

            LinearFill(x, y);
            FloodFillRange range;

            while (mRanges.size() > 0) {
                range = mRanges.remove();
                int downPxIdx = (mWidth * (range.Y + 1)) + range.startX;
                int upPxIdx = (mWidth * (range.Y - 1)) + range.startX;
                int upY = range.Y - 1;
                int downY = range.Y + 1;

                for (int i = range.startX; i <= range.endX; i++) {
                    if (range.Y > 0 && (!mPixelsChecked[upPxIdx]) && CheckPixel(upPxIdx)) LinearFill(i, upY);
                    if (range.Y < (mHeight - 1) && (!mPixelsChecked[downPxIdx]) && CheckPixel(downPxIdx)) LinearFill(i, downY);
                    downPxIdx++;
                    upPxIdx++;
                }
            }

            mImage.setPixels(mPixels, 0, mWidth, 0, 0, mWidth, mHeight);
        }

        protected void LinearFill(int x, int y) {
            int lFillLoc = x;
            int pxIdx = (mWidth * y) + x;

            while (true) {
                mPixels[pxIdx] = mFillColor;
                mPixelsChecked[pxIdx] = true;
                lFillLoc--;
                pxIdx--;

                if (lFillLoc < 0 || (mPixelsChecked[pxIdx]) || !CheckPixel(pxIdx)) {
                    break;
                }
            }

            lFillLoc++;
            int rFillLoc = x;

            pxIdx = (mWidth * y) + x;

            while (true) {
                mPixels[pxIdx] = mFillColor;
                mPixelsChecked[pxIdx] = true;

                rFillLoc++;
                pxIdx++;

                if (rFillLoc >= mWidth || mPixelsChecked[pxIdx] || !CheckPixel(pxIdx)) {
                    break;
                }
            }

            rFillLoc--;

            FloodFillRange r = new FloodFillRange(lFillLoc, rFillLoc, y);

            mRanges.offer(r);
        }

        protected boolean CheckPixel(int px) {
            int red = (mPixels[px] >>> 16) & 0xff;
            int green = (mPixels[px] >>> 8) & 0xff;
            int blue = mPixels[px] & 0xff;
            int alpha = (Color.alpha(mPixels[px]));

            return (red >= (mStartColor[0] - mTolerance[0]) && red <= (mStartColor[0] + mTolerance[0])
                    && green >= (mStartColor[1] - mTolerance[1]) && green <= (mStartColor[1] + mTolerance[1])
                    && blue >= (mStartColor[2] - mTolerance[2]) && blue <= (mStartColor[2] + mTolerance[2])
                    && alpha >= (mStartColor[3] - mTolerance[3]) && alpha <= (mStartColor[3] + mTolerance[3]));
        }

        protected class FloodFillRange {
            public int startX;
            public int endX;
            public int Y;

            public FloodFillRange(int startX, int endX, int y) {
                this.startX = startX;
                this.endX = endX;
                this.Y = y;
            }
        }
    }
}
