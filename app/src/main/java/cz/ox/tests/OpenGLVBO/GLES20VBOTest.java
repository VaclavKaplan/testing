package cz.ox.tests.OpenGLVBO;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;


public class GLES20VBOTest extends Activity {

    private Runnable mUpdateRunnable;

    private String mText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final TextView tv = new TextView(this);
        tv.setText("Ahoj");

        mUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                tv.setText(mText);
                tv.invalidate();
            }
        };

        GLSurfaceView view = new GLSurfaceView(this);
        view.setEGLContextClientVersion(2);
        view.setRenderer(new GDC11Renderer(new GDC11Renderer.UpdateFps() {
            @Override
            public void update(String text) {
                mText = text;
                runOnUiThread(mUpdateRunnable);
            }
        }));
        view.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        setContentView(view);
        addContentView(tv, lp);
    }

}

