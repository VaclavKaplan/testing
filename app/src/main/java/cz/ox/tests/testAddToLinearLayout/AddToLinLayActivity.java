package cz.ox.tests.testAddToLinearLayout;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cz.ox.tests.R;

public class AddToLinLayActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_view_to_lin_lay);

        LinearLayout dynamicContent = findViewById(R.id.dynamic_content);

        LayoutInflater inflater = LayoutInflater.from(this);

        int numbers[] = {654, 213, 113, 213, 23, 45,84 };

        for (int number : numbers) {
            View newRow = inflater.inflate(R.layout.item_view_to_lin_lay, dynamicContent, false);
            TextView binaryString = newRow.findViewById(R.id.binary_string);
            binaryString.setText(Integer.toBinaryString(number));

            dynamicContent.addView(newRow);
        }
    }

}
