package cz.ox.tests.Mix;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import java.util.Timer;
import java.util.TimerTask;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class MixActivity extends Activity implements PlusOneFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mix);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.container, PlusOneFragment.newInstance("", ""), null);
        ft.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d("plusButton",uri.toString());
    }
}
