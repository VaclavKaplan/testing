package cz.ox.tests.testOgg;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 *
 * linux command cat looks working for ogg files
 * cat sound1.ogg sound2.ogg > newSound.ogg
 */
public class TestOggActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_ogg);

        TextView tv;

        tv = (TextView) findViewById(R.id.text);

        try {
            File baseDir = Environment.getExternalStorageDirectory();
            String audioPath = baseDir.getAbsolutePath()+File.separator+"a"+File.separator+"n.ogg";
            tv.setText(audioPath);

            FileInputStream fis = new FileInputStream(audioPath);
            FileDescriptor fd = fis.getFD();

            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(fd);
            mediaPlayer.prepare();
            mediaPlayer.start();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
