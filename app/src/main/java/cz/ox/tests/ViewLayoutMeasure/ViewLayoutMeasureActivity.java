package cz.ox.tests.ViewLayoutMeasure;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ViewGroup;

import cz.ox.tests.R;

/**
 * Created by Crusty on 18.8.2015.
 */
public class ViewLayoutMeasureActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_layout_measure);
    }
}


