package cz.ox.tests.SocketChannels;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Console;
import java.io.IOException;
import java.net.InetSocketAddress;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class SocketChannelsActivity extends Activity {

    private Server mServer = null;
    private Client mClient = null;
    private EditText mMessage;

    private HttpServer mHttpServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_socket_channels);

        Button b;

        b = (Button) findViewById(R.id.server);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mServer == null) {
                    mServer = new Server();
                    mServer.start();
                }
            }
        });

        b = (Button) findViewById(R.id.client);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mClient == null) {
                    mClient = new Client("Hello there");
                    mClient.start();
                }
            }
        });

        mMessage = (EditText) findViewById(R.id.message);

        b = (Button) findViewById(R.id.client_send);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClient == null)
                    return;

                mClient.send(mMessage.getText().toString());
            }
        });

        Console console = System.console();
        if(console != null) {
            console.format("Hello");
        }

        final EditText webServerMessage = (EditText) findViewById(R.id.webserver_message);
        b = (Button) findViewById(R.id.http_server);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHttpServer != null)
                    return;

                try {
                    //InetSocketAddress address = new InetSocketAddress("127.0.0.1", 2222);
                    InetSocketAddress address = new InetSocketAddress("192.168.206.101", 2222);
                    mHttpServer = new HttpServer(address, webServerMessage);
                    new Thread(mHttpServer).start();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
