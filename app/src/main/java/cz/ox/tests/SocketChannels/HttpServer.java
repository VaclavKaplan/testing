package cz.ox.tests.SocketChannels;


import android.util.Log;
import android.widget.EditText;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 */

public class HttpServer implements Runnable {

    private final boolean DEBUG = true;

    private final EditText mMessage;
    private Charset charset = Charset.forName("UTF-8");
    private CharsetEncoder encoder = charset.newEncoder();
    private Selector selector = Selector.open();
    private ServerSocketChannel server = ServerSocketChannel.open();
    private boolean isRunning = true;


    /**
     */

    protected HttpServer(InetSocketAddress address, EditText webServerMessage) throws IOException {
        mMessage = webServerMessage;
        server.socket().bind(address);
        if(DEBUG) {
            Log.d("@", "Server binded on " + address);
        }
        server.configureBlocking(false);
        server.register(selector, SelectionKey.OP_ACCEPT);
    }

    /**
     */

    @Override
    public final void run() {

        while (isRunning) {
            // FIXME
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                selector.selectNow();
                Iterator<SelectionKey> i = selector.selectedKeys().iterator();

                while (i.hasNext()) {
                    SelectionKey key = i.next();
                    i.remove();
                    if (!key.isValid()) {
                        continue;
                    }

                    try {
                        // get a new connection
                        if (key.isAcceptable()) {
                            // accept them
                            SocketChannel client = server.accept();
                            // non blocking please
                            client.configureBlocking(false);
                            // show out intentions
                            client.register(selector, SelectionKey.OP_READ);
                            // read from the connection
                        }
                        else if (key.isReadable()) {
                            //  get the client
                            SocketChannel client = (SocketChannel) key.channel();
                            // get the session
                            HTTPSession session = (HTTPSession) key.attachment();
                            // create it if it doesnt exist
                            if (session == null) {
                                session = new HTTPSession(client);
                                key.attach(session);
                            }

                            // get more data
                            session.readData();
                            // decode the message
                            String line;
                            while ((line = session.readLine()) != null) {
                                // check if we have got everything
                                if (line.isEmpty()) {
                                    HTTPRequest request = new HTTPRequest(session.readLines.toString());
                                    session.sendResponse(handle(session, request));
                                    session.close();
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        System.err.println("Error handling client: " + key.channel());
                        if (DEBUG) {
                            ex.printStackTrace();
                        }
                        else {
                            System.err.println(ex);
                            System.err.println("\tat " + ex.getStackTrace()[0]);
                        }

                        if (key.attachment() instanceof HTTPSession) {
                            ((HTTPSession) key.attachment()).close();
                        }
                    }
                }
            }
            catch (IOException ex) {
                // call it quits
                shutdown();
                // throw it as a runtime exception so that Bukkit can handle it
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     *
     */
    protected HTTPResponse handle(HTTPSession session, HTTPRequest request) throws IOException {

        HTTPResponse response = new HTTPResponse();

        StringBuilder page = new StringBuilder();
        page.append("<html>");
        page.append("<body>");
        page.append("My Web Server <b>ROCKS</b><br>");
        page.append("<h2>:-)</h2>");
        page.append("by Crusty<br>");
        page.append("<br>");
        page.append(mMessage.getText().toString());
        page.append("<br>");
        page.append("</body>");
        page.append("</html>");

        response.setContent(page.toString().getBytes());

        return response;
    }

    /**
     *
     */

    public final void shutdown() {

        isRunning = false;

        try {
            selector.close();
            server.close();
        }
        catch (IOException ex) {
            // do nothing, its game over
        }
    }

    public static class HTTPRequest {

        private final String raw;
        private String method;
        private String location;
        private String version;
        private Map<String, String> headers = new HashMap<String, String>();

        public HTTPRequest(String raw) {
            this.raw = raw;
            parse();
        }

        private void parse() {
            // parse the first line
            StringTokenizer tokenizer = new StringTokenizer(raw);
            method = tokenizer.nextToken().toUpperCase();
            location = tokenizer.nextToken();
            version = tokenizer.nextToken();
            // parse the headers
            String[] lines = raw.split("\r\n");
            for (int i = 1; i < lines.length; i++) {
                String[] keyVal = lines[i].split(":", 2);
                headers.put(keyVal[0], keyVal[1]);
            }
        }

        public String getMethod() {
            return method;
        }

        public String getLocation() {
            return location;
        }

        public String getHead(String key) {
            return headers.get(key);
        }
    }

    public static class HTTPResponse {

        private String version = "HTTP/1.1";
        private int responseCode = 200;
        private String responseReason = "OK";
        private Map<String, String> headers = new LinkedHashMap<String, String>();
        private byte[] content;

        private void addDefaultHeaders() {
            headers.put("Date", new Date().toString());
            headers.put("Server", "Android WebServer by www.2ox.cz");
            headers.put("Connection", "close");
            headers.put("Content-Length", Integer.toString(content.length));
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public String getResponseReason() {
            return responseReason;
        }

        public void setResponseReason(String responseReason) {
            this.responseReason = responseReason;
        }

        public String getHeader(String header) {
            return headers.get(header);
        }

        public byte[] getContent() {
            return content;
        }

        public void setContent(byte[] content) {
            this.content = content;
        }

        public void setHeader(String key, String value) {
            headers.put(key, value);
        }
    }

    public final class HTTPSession {

        private final SocketChannel channel;
        private final ByteBuffer buffer = ByteBuffer.allocate(2048);
        private final StringBuilder readLines = new StringBuilder();
        private int mark = 0;

        public HTTPSession(SocketChannel channel) {
            this.channel = channel;
        }

        /**
         *
         */
        public String readLine() throws IOException {

            StringBuilder sb = new StringBuilder();
            int l = -1;

            while (buffer.hasRemaining()) {
                char c = (char) buffer.get();
                sb.append(c);
                if (c == '\n' && l == '\r') {
                    // mark our position
                    mark = buffer.position();
                    // append to the total
                    readLines.append(sb);
                    // return with no line separators
                    return sb.substring(0, sb.length() - 2);
                }
                l = c;
            }
            return null;
        }

        /**
         *
         */
        public void readData() throws IOException {

            buffer.limit(buffer.capacity());
            int read = channel.read(buffer);
            if (read == -1) {
                throw new IOException("End of stream");
            }
            buffer.flip();
            buffer.position(mark);
        }

        private void writeLine(String line) throws IOException {
            channel.write(encoder.encode(CharBuffer.wrap(line + "\r\n")));
        }

        public void sendResponse(HTTPResponse response) {
            response.addDefaultHeaders();
            try {
                writeLine(response.version + " " + response.responseCode + " " + response.responseReason);
                for (Map.Entry<String, String> header : response.headers.entrySet()) {
                    writeLine(header.getKey() + ": " + header.getValue());
                }
                writeLine("");
                channel.write(ByteBuffer.wrap(response.content));
            }
            catch (IOException ex) {
                // slow silently
            }
        }

        public void close() {
            try {
                channel.close();
            }
            catch (IOException ex) {
            }
        }
    }
}
