package cz.ox.tests.DownloadManager;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.security.acl.AclNotFoundException;

import cz.ox.tests.R;

/**
 * Created by Crusty on 4.7.2015.
 */
public class DownloadManagerActivity extends Activity {

    private DownloadManager mDownloadManager = null;
    private long mLastDownloadId = -1L;

    private TextView mLog;
    private EditText mUrl;
    private Spinner mProtocol;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_manager);

        mProtocol = (Spinner) findViewById(R.id.protocol);

        mUrl = (EditText) findViewById(R.id.url);
        mLog = (TextView) findViewById(R.id.log);

        mDownloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(onNotificationClick,
                new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(onComplete);
        unregisterReceiver(onNotificationClick);
    }

    private static final String NEW_LINE = "\n";

    private void addLogMessage(String msg) {
        mLog.append(getTime());
        mLog.append(msg);
        mLog.append(NEW_LINE);
        ScrollView sv = (ScrollView) mLog.getParent();
        sv.fullScroll(ScrollView.FOCUS_DOWN);
    }

    public void startDownload(View v) {

        String url = (String) mProtocol.getSelectedItem();
        url += mUrl.getText().toString();
        if(url.length() < 8)
            url = "http://www.2ox.cz/index_soubory/2ox_logo_cut.png";
        final String splits[] = url.split("/");
        final String fileName = splits[splits.length - 1];
        Uri uri = Uri.parse(url);

        Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .mkdirs();

        try {
            mLastDownloadId = mDownloadManager.enqueue(new DownloadManager.Request(uri)
                            .setTitle(fileName)
                            .setDescription(url)
                            .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            );
        }
        catch (Exception ex) {
            addLogMessage(ex.getMessage());
            return;
        }

        v.setEnabled(false);
        addLogMessage("URL: "+url+", File: " + fileName);
    }

    public String queryStatus() {
        Cursor c = mDownloadManager.query(new DownloadManager.Query().setFilterById(mLastDownloadId));

        if (c == null) {
            return "Download #"+mLastDownloadId+" not found!";
        }

        c.moveToFirst();

        Log.d(getClass().getName(), "COLUMN_ID: " +
                c.getLong(c.getColumnIndex(DownloadManager.COLUMN_ID)));
        Log.d(getClass().getName(), "COLUMN_BYTES_DOWNLOADED_SO_FAR: " +
                c.getLong(c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)));
        Log.d(getClass().getName(), "COLUMN_LAST_MODIFIED_TIMESTAMP: " +
                c.getLong(c.getColumnIndex(DownloadManager.COLUMN_LAST_MODIFIED_TIMESTAMP)));
        Log.d(getClass().getName(), "COLUMN_LOCAL_URI: " +
                c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)));
        Log.d(getClass().getName(), "COLUMN_STATUS: " +
                c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS)));
        Log.d(getClass().getName(), "COLUMN_REASON: " +
                c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));

        return statusMessage(c);
    }

    public void viewDownloads(View v) {
        startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
    }

    public void launchFileManager(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        try {
            startActivity(intent);
        }
        catch (ActivityNotFoundException ex) {
            addLogMessage(ex.getMessage());
        }
    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;

            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;

            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;

            case DownloadManager.STATUS_RUNNING:
                msg = "Download in progress!";
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;

            default:
                msg = "Download is nowhere in sight";
                break;
        }

        return (msg);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            findViewById(R.id.start).setEnabled(true);
            addLogMessage(queryStatus());
        }
    };

    BroadcastReceiver onNotificationClick = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, queryStatus(), Toast.LENGTH_LONG).show();
        }
    };

    private String getTime() {
        Time time = new Time();
        time.setToNow();
        return time.format("%H:%M.%S ");
    }
}
