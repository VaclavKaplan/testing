package cz.ox.tests.DesignSupportLib;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import cz.ox.tests.R;

/**
 * Created by Crusty on 4.7.2015.
 */
public class DesignSupportActivity extends AppCompatActivity {

    View mRootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_support);
        mRootView = findViewById(R.id.root);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextInputLayout edit = (TextInputLayout) findViewById(R.id.edit);
        edit.setError("Error msg");
        edit.setErrorEnabled(true);
        edit.setHint("Hint msg");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.design_support, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                Snackbar.make(mRootView, "Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Cool Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(v.getContext(), "Old Toast", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setActionTextColor(Color.RED)
                        .show();
                return true;

            case R.id.action_settings:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
