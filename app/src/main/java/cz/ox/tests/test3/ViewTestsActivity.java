package cz.ox.tests.test3;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import java.util.Timer;
import java.util.TimerTask;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class ViewTestsActivity extends Activity {

    ViewFlipper vf;
    TextSwitcher ts;
    int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test3);

        ts = (TextSwitcher) findViewById(R.id.textSwitcher);
        ts.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                return new TextView(ViewTestsActivity.this);
            }
        });

        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        ts.setInAnimation(in);
        ts.setOutAnimation(out);

        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        num++;
                        ts.setText("test " + num);

                    }
                });

            }
        }, 1000, 1000);


        vf = (ViewFlipper) findViewById(R.id.viewFlipper);
        vf.setFlipInterval(1000);
        vf.setAutoStart(true);

        vf.setInAnimation(this, R.anim.abc_slide_in_top);
        vf.setOutAnimation(this, R.anim.abc_slide_out_bottom);

        final int imgs[] = {R.drawable.photo1, R.drawable.photo2, R.drawable.photo3,
                            R.drawable.photo4, R.drawable.photo5, R.drawable.photo6, };
        ImageView iv;
        for(int id : imgs) {
            iv = new ImageView(this);
            iv.setImageResource(id);
            vf.addView(iv);
        }
    }
}
