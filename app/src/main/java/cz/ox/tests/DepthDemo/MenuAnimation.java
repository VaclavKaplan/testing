package cz.ox.tests.DepthDemo;

public interface MenuAnimation {
      void animateTOMenu();
      void revertFromMenu();
      void exitFromMenu();
}
