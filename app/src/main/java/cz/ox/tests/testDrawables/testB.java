package cz.ox.tests.testDrawables;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import cz.ox.tests.R;

public class testB extends Activity implements OnClickListener {

    ImageView chosenImageView;
    Button choosePicture;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_drawables_b);

        chosenImageView = (ImageView) this.findViewById(R.id.ChosenImageView);
        choosePicture = (Button) this.findViewById(R.id.ChoosePictureButton);

        choosePicture.setOnClickListener(this);

        Button test2 = (Button) findViewById(R.id.test2);
        test2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView img = (ImageView)findViewById(R.id.imgTest2);
                img.setBackgroundResource(R.drawable.test2);
                // Get the AnimationDrawable object.
                AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();
                // Start the animation (looped playback by default).
                frameAnimation.start();
            }
        });

        Button test3 = (Button) findViewById(R.id.test3);
        test3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void onClick(View v) {
        Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(choosePictureIntent, 0);
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            Uri imageFileUri = intent.getData();

            Display currentDisplay = getWindowManager().getDefaultDisplay();
            int dw = currentDisplay.getWidth();
            int dh = currentDisplay.getHeight() / 2 - 100;

            try {
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                Bitmap bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        imageFileUri), null, bmpFactoryOptions);

                bmpFactoryOptions.inSampleSize = 2;
                bmpFactoryOptions.inJustDecodeBounds = false;
                bmp = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        imageFileUri), null, bmpFactoryOptions);
                Bitmap alteredBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp
                        .getHeight(), bmp.getConfig());
                Canvas canvas = new Canvas(alteredBitmap);
                Paint paint = new Paint();

                Matrix matrix = new Matrix();
                // Rotate around Center Point
                matrix.setRotate(15, bmp.getWidth() / 2, bmp.getHeight() / 2);

                canvas.drawBitmap(bmp, matrix, paint);

                ImageView alteredImageView = (ImageView) this.findViewById(R.id.AlteredImageView);
                alteredImageView.setImageBitmap(alteredBitmap);

                chosenImageView.setImageBitmap(bmp);

            } catch (Exception e) {
                Log.v("ERROR", e.toString());
            }
        }
    }
}