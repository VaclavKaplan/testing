package cz.ox.tests.Touby;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;

import cz.ox.tests.R;

/**
 * Created by Crusty on 13.7.2015.
 */
public class MainActivity extends Activity {

    Button btn1, btn2, btn3, btn4;
    public int level = 1;
    public String[] kodLevelu = {"1"};
    public String odpovedLevelu;
    Casovac timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_touby_main);

        // levely
        View.OnClickListener onLvlClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int level = 0;
                switch (v.getId()) {
                    case R.id.lvl1:
                        level = 1;
                        break;
                    case R.id.lvl2:
                        level = 2;
                        break;
                    case R.id.lvl3:
                        level = 3;
                        break;
                }

                vyberLevel(level);
            }
        };

        Button b;
        b = (Button) findViewById(R.id.lvl1);
        b.setOnClickListener(onLvlClick);
        b = (Button) findViewById(R.id.lvl2);
        b.setOnClickListener(onLvlClick);
        b = (Button) findViewById(R.id.lvl3);
        b.setOnClickListener(onLvlClick);

        // tlacitka
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);

        nastavCernouBarvu();
    }

    public void vyberLevel(int level) {
        switch (level) {
            case 1:
                casovac(btn1, btn3, btn4);
                break;

            case 2:
                casovac(btn2, btn3, btn4);
                break;

            case 3:
                casovac(btn1, btn2, btn4);
                break;
        }
    }

    public void casovac(Button... bttns) {
        timer = new Casovac(1000, 1000, bttns);
        timer.start();
    }

    public void nastavCernouBarvu() {
        btn1.setBackgroundColor(Color.BLACK);
        btn2.setBackgroundColor(Color.BLACK);
        btn3.setBackgroundColor(Color.BLACK);
        btn4.setBackgroundColor(Color.BLACK);
    }

    public class Casovac extends CountDownTimer {

        private final Button mTlacitka[];
        private int mAktualniTlacitko = 0;

        public Casovac(long millisInFuture, long countDownInterval, Button tlacitka[]) {
            super(millisInFuture, countDownInterval);
            mTlacitka = tlacitka;

            mTlacitka[mAktualniTlacitko].setBackgroundColor(Color.RED);
        }

        @Override
        public void onFinish() {
            // vrat zpet cernou barvu
            mTlacitka[mAktualniTlacitko].setBackgroundColor(Color.BLACK);
            // prejdem na dalsi
            mAktualniTlacitko++;
            if(mAktualniTlacitko < mTlacitka.length) {
                mTlacitka[mAktualniTlacitko].setBackgroundColor(Color.RED);
                // a na dalsi
                this.start();
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}