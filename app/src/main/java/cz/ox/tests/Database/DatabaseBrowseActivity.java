package cz.ox.tests.Database;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cz.ox.tests.R;

/**
 * testing
 */
public class DatabaseBrowseActivity extends Activity {

    private ViewGroup mRootView;

    private TextView mText;
    private EditText mEdit;
    private Button mButtonSelect;
    private Button mButtonOther;

    private SQLiteDatabase mDatabase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v(DatabaseBrowseActivity.class.getSimpleName(), "onCreate");

        mDatabase = openOrCreateDatabase("database.db", MODE_PRIVATE, null);

        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_browse_database, null, false);

        mText = (TextView) mRootView.findViewById(R.id.text);
        mEdit = (EditText) mRootView.findViewById(R.id.edit);
        mEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEND || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    mButtonSelect.performClick();
                    return true;
                }
                return false;
            }
        });

        mButtonSelect = (Button) mRootView.findViewById(R.id.button_select);
        mButtonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = mEdit.getText().toString();
                Cursor cursor = null;
                try {
                    cursor = mDatabase.rawQuery(query, null);
                } catch (SQLiteException ex) {
                    Log.d("SQLite Ex", ex.getMessage());
                }
                if(cursor != null) {
                    String str = Debug.cursorToString(cursor);
                    if(str.length() == 0)
                        mText.setText("result is empty");
                    else
                        mText.setText(str);
                    cursor.close();
                }
                else {
                    mText.setText("Cursor == null");
                }
            }
        });

        mButtonOther = (Button) mRootView.findViewById(R.id.button_other);
        mButtonOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = mEdit.getText().toString();
                boolean state = true;
                try {
                    mDatabase.execSQL(query);
                }
                catch (Exception ex) {
                    state = false;
                }
                mText.setText(state+"\n"+query);
            }
        });

        setContentView(mRootView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDatabase.close();
    }
}