package cz.ox.tests.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.List;

/**
 * Created by Crusty on 19.10.2015.
 *
 * debug outputs, testings, helpers, ...
 */
public class Debug {
    public static void showFragments(FragmentManager fm) {
        List<Fragment> frags = fm.getFragments();
        for (int x = 0; x < frags.size(); x++) {
            Log.d("FragmentManager", " - " + frags.get(x));
        }
    }

    public static String printSocketChannelAddresses(SocketChannel socketChannel) {
        String ret = "N/A";
        if(socketChannel == null)
            return ret;

        Socket socket = socketChannel.socket();
        ret = "inetAddress "+socket.getInetAddress();
        ret += " localAddress "+socket.getLocalAddress();
        ret += " localSocketAddress "+socket.getLocalSocketAddress();
        ret += " remoteSocketAddress "+socket.getRemoteSocketAddress();

        return ret;
    }

    public static String cursorToString(Cursor cursor) {
        StringBuilder sb = new StringBuilder();

        try {
            int count = cursor.getCount();
            String[] columnNames = cursor.getColumnNames();
            for (int x = 0; x < count; x++) {
                cursor.moveToPosition(x);
                for (int c = 0; c < columnNames.length; c++) {
                    if (cursor.getType(c) == Cursor.FIELD_TYPE_BLOB)
                        sb.append(columnNames[c]).append(": ").append(byteToHex(cursor.getBlob(c))).append(", ");
                    else
                        sb.append(columnNames[c]).append(": ").append(cursor.getString(c)).append(", ");
                }
                sb.append("\n\n");
            }
        }
        catch (SQLiteException ex) {
            sb.setLength(0);
            sb.append("Exception: ");
            sb.append(ex.getMessage());
        }

        return sb.toString();
    }

    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String byteToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
