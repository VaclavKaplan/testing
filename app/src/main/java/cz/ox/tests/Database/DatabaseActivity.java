package cz.ox.tests.Database;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

import cz.ox.tests.R;

public class DatabaseActivity extends Activity {

    private SQLiteDatabase mDatabase;

    private int mCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_database);

        mDatabase = openOrCreateDatabase("database.db", MODE_PRIVATE, null);

        mDatabase.execSQL("CREATE TABLE IF NOT EXISTS user (name TEXT, image BLOB);");

        final TextView text = (TextView) findViewById(R.id.text);
        text.setMovementMethod(new ScrollingMovementMethod());

        Button clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.execSQL("DELETE FROM user");
                text.setText("");

                showUsers(mDatabase, text);
            }
        });

        Button insert = (Button) findViewById(R.id.insert);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setText("");

                Image image = new Image();
                image.a = ++mCounter;
                image.b = "Hello";

                byte[] bytes = Image.saveObject(image);
                text.append("Save blob");
                text.append("\n");
                text.append(bytesToHex(bytes));
                text.append("\n\n");

                mDatabase.execSQL("INSERT INTO user (name, image) VALUES ('Crusty', X'"+bytesToHex(bytes)+"');");

                showUsers(mDatabase, text);
            }
        });

        showUsers(mDatabase, text);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mDatabase != null)
            mDatabase.close();
    }

    private void showUsers(SQLiteDatabase db, TextView text) {
        Cursor cursor = db.rawQuery("SELECT * FROM user", null);
        try {
            text.append("SELECT ");
            text.append(""+cursor.getCount());
            text.append("\n");

            String name;
            byte blob[];
            Image loaded;
            while (cursor.moveToNext()) {
                name = cursor.getString(0);
                blob = cursor.getBlob(1);

                text.append(name);
                text.append(", ");
                text.append(bytesToHex(blob));
                text.append("\n");

                loaded = (Image) Image.loadObject(blob);
                text.append(""+loaded.a);
                text.append(", ");
                text.append(loaded.b);
                text.append("\n");

                text.append("\n");
            }

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
