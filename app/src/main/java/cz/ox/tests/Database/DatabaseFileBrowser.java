package cz.ox.pokladna.Database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.io.File;

/**
 * Created by Crusty on 10.08.2016.
 *
 * tool to browse and operate with database from file, given path
 */
public class DatabaseFileBrowser {

    private SQLiteDatabase mDatabase;

    public boolean open(File databasePath) {
        try {
            mDatabase = SQLiteDatabase.openDatabase(databasePath.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
        }
        catch (SQLiteException ex) {
            ex.printStackTrace();
            return false;
        }
        if(mDatabase == null)
            return false;

        return true;
    }

    public void close() {
        if(mDatabase == null)
            return;

        mDatabase.close();
        mDatabase = null;
    }

    public SQLiteDatabase getDatabase() {
        return mDatabase;
    }
}
