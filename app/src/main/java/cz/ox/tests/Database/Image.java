package cz.ox.tests.Database;

import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by Crusty on 27.10.2016.
 */

class Image implements Serializable {
    int a;
    String b;

    public static byte[] saveObject(Image image) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(output);
            oos.writeObject(image);
            oos.flush();
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return output.toByteArray();
    }

    public static Object loadObject(byte[] bytes) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
            Object o = ois.readObject();
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
