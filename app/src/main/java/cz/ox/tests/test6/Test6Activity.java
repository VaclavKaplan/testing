package cz.ox.tests.test6;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class Test6Activity extends Activity {


    private SocketServerThread mServer;

    private SocketClientThread mSocketClientThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test6);

        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Test6Activity.this, "Hello World", Toast.LENGTH_LONG).show();
            }
        });


        final EditText ip = (EditText) findViewById(R.id.ip);

        TextView text = (TextView) findViewById(R.id.text);
        String str = "";
        text.setText(str);

        b = (Button) findViewById(R.id.connect);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    URL url;
                    //url = new URL("http://192.168.112.1:2222");
                    url = new URL("http://"+ip.getText().toString()+":2222");

                    NetworkThread thread = new NetworkThread(url);
                    thread.start();
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });

        b = (Button) findViewById(R.id.socket);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mSocketClientThread = new SocketClientThread("127.0.0.1", 2222);
                mSocketClientThread = new SocketClientThread("192.168.112.1", 2222);
                mSocketClientThread.start();
            }
        });

        b = (Button) findViewById(R.id.socket_send);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSocketClientThread.send("Hello Dolly");
            }
        });

        mServer = new SocketServerThread(new OnClientAccepted() {
            @Override
            public void onAccept(SocketClientThread client) {
                mSocketClientThread = client;
            }
        });
        mServer.start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mServer.kill();
    }
}








