package cz.ox.tests.test6;

import android.util.Log;

import org.apache.http.impl.SocketHttpServerConnection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by admin on 17. 1. 2016.
 */
class SocketServerThread extends Thread {

    private boolean mAccepting = true;
    private ServerSocket mServerSocket;
    private OnClientAccepted mOnClientAccepted;

    public SocketServerThread(OnClientAccepted onClientAccepted) {
        mOnClientAccepted = onClientAccepted;
    }

    @Override
    public void run() {
        try {
            mServerSocket = new ServerSocket(2222);

            while (mAccepting) {
                Socket clientSocket = mServerSocket.accept();

                Log.d("@ accepted", clientSocket.toString());
                SocketClientThread client = new SocketClientThread(clientSocket);
                client.start();
                if(mOnClientAccepted != null) {
                    mOnClientAccepted.onAccept(client);
                }
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void kill() {
        mAccepting = false;
        try {
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
