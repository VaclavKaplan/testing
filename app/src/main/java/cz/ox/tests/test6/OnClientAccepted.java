package cz.ox.tests.test6;

/**
 * Created by admin on 20. 1. 2016.
 */
public interface OnClientAccepted {
    void onAccept(SocketClientThread client);
}
