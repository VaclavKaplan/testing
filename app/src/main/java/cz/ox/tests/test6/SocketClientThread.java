package cz.ox.tests.test6;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by admin on 17. 1. 2016.
 */
class SocketClientThread extends Thread {

    private int mPort;
    private String mAddress;
    private Socket mSocket;
    private boolean mReading = true;
    private int numRead;
    private boolean mCreate = false;

    public SocketClientThread(Socket clientSocket) {
        mSocket = clientSocket;
    }

    public SocketClientThread(String address, int port) {
        if(address != null && port > 0) {
            mCreate = true;
            mAddress = address;
            mPort = port;
        }
    }

    @Override
    public void run() {
        try {
            if(mCreate) {
                mSocket = new Socket(mAddress, mPort);
            }
            // send welcome message
            //sendCommand(0);

            StringBuilder sb = new StringBuilder();
            InputStream input = mSocket.getInputStream();
            byte[] buffer = new byte[20];
            while (mReading) {
                numRead = input.read(buffer, 0, buffer.length);
                //if(numRead == -1)
                  //  continue;
                sb.append(new String(buffer, 0, numRead));
                Log.d("@ read", sb.toString());
                /*
                if(read.contentEquals("1")) {
                    sendCommand(1);
                }
                */
            }
            Log.d("@ close", ""+numRead);
            input.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendCommand(int cmd) throws JSONException, IOException {
        Log.d("@", "send command "+cmd);

        JSONObject json = new JSONObject();
        json.put("cmd", cmd);
        json.put("user", "ROOT");

        sendMessage(json.toString());
    }

    private void sendMessage(String message) throws JSONException, IOException {
        OutputStream os = mSocket.getOutputStream();
        os.write(message.getBytes());
        os.flush();
    }

    public void send(String message) {
        try {
            sendMessage(message);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
