package cz.ox.tests.test6;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by admin on 17. 1. 2016.
 */
class NetworkThread extends Thread {

    private final URL mUrl;

    NetworkThread(URL url) {
        mUrl = url;
    }

    @Override
    public void run() {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) mUrl.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String read = new String(readStream(in));
                Log.d("@ read", read);
            }
            finally {
                Log.d("@", "closing connection ");
                urlConnection.disconnect();
            }

        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] readStream(InputStream in) {
        int BUFFER_SIZE = 8;
        byte[] buffer = new byte[BUFFER_SIZE];
        try {
            int read = in.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }
}
