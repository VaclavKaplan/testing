package cz.ox.tests._Launcher.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

import cz.ox.tests.R;

public class ActivitiesFragment extends BaseFragment {

    public static final String PREFERENCES_LANCHER = "prefs_launcher";
    public static final String PREFS_KEY_LAUNCH_AFTER_START = "key_launch_after_start";

    private ViewGroup mRootView;

    ArrayList<ActivityInfo> mActivities = null;
    private int mCategory = CategoriesFragment.CATEGORY_UNDEFINED;

    LauncherAdapter mAdapter;

    public ActivitiesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        Bundle args = getArguments();
        if(args != null) {
            mCategory = args.getInt("category", CategoriesFragment.CATEGORY_UNDEFINED);
        }
        getActivitiesByCategory(mCategory);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //inflater.inflate(R.menu., menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v(ActivitiesFragment.class.getSimpleName(), "onCreateView");

        final Context context = getActivity();

        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_launcher_activities, container, false);

        ImageView iv;

        TextView category = (TextView) mRootView.findViewById(R.id.category);
        if(mCategory != CategoriesFragment.CATEGORY_UNDEFINED)
            category.setText(mCategory);
        else
            category.setText(R.string.category_undefined);

        iv = (ImageView) mRootView.findViewById(R.id.back);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        ListView list = (ListView) mRootView.findViewById(R.id.list);
        View footerView = inflater.inflate(R.layout.launcher_list_footer, null);
        ImageView logo = (ImageView) footerView.findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = AnimationUtils.loadAnimation(v.getContext(),
                        R.anim.bounce);
                v.startAnimation(anim);
            }
        });
        list.addFooterView(footerView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActivities == null)
                    return;
                if (position >= mActivities.size()) {
                    Log.d("", "Footer click");
                    return;
                }

                ActivityInfo ai = mActivities.get(position);

                Intent intent = new Intent();
                intent.setClassName(getActivity(), ai.name);

                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActivities == null)
                    return false;
                if (position >= mActivities.size()) {
                    Log.d("", "Footer click");
                    return false;
                }

                ActivityInfo ai = mActivities.get(position);
                final SharedPreferences prefs = getActivity().getSharedPreferences(PREFERENCES_LANCHER, Context.MODE_PRIVATE);
                String newAct = "";
                final String current = prefs.getString(PREFS_KEY_LAUNCH_AFTER_START, newAct);
                if (current.compareTo(ai.name) != 0) {
                    newAct = ai.name;
                }
                prefs.edit().putString(PREFS_KEY_LAUNCH_AFTER_START, newAct).commit();

                mAdapter.notifyDataSetChanged();

                return true;
            }
        });

        mAdapter = new LauncherAdapter(getActivity(),
                inflater, getActivity().getPackageManager());
        mAdapter.addAll(mActivities);
        list.setAdapter(mAdapter);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getActivitiesByCategory(int category) {
        try {
            PackageInfo pi = getActivity().getPackageManager()
                    .getPackageInfo("cz.ox.tests",
                            PackageManager.GET_ACTIVITIES | PackageManager.GET_META_DATA);
            if(pi == null)
                return;
            mActivities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(mActivities == null)
            return;

        // get only by category
        // remove splash, launcher activities
        ActivityInfo ai;
        ListIterator li = mActivities.listIterator();
        while(li.hasNext()) {
            ai = (ActivityInfo) li.next();

            // if undefined category
            if(mCategory == CategoriesFragment.CATEGORY_UNDEFINED) {
                if(ai.metaData != null
                        && ai.metaData.getInt("category") != 0)
                    li.remove();
            }
            else {
                if (ai.metaData == null) {
                    li.remove();
                    continue;
                }
                if (category != ai.metaData.getInt("category")) {
                    li.remove();
                    continue;
                }
            }


            if(ai.name.contains("LauncherActivity")) {
                li.remove();
                continue;
            }

            if(ai.name.contains("SplashActivity")) {
                li.remove();
                continue;
            }
        }
    }

    private class LongOperation extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Integer... par) {
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {}

        @Override
        protected void onPostExecute(Void v) {
            Log.d("", "@@@ done");
            if(isAdded() == false)
                return;
        }
    }

    public class LauncherAdapter extends ArrayAdapter<ActivityInfo> {

        private final LayoutInflater mInflater;
        private PackageManager mPackageManager;

        public LauncherAdapter(Context context, LayoutInflater infalter, PackageManager manager) {
            super(context, 0);
            mInflater = infalter;
            mPackageManager = manager;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.launcher_list_item, null);
            }

            ActivityInfo item = getItem(position);

            /*if(position % 2 == 0) {
                convertView.setBackgroundColor(0xffd0d0d0);
            } else {
                convertView.setBackgroundDrawable(null);
            }*/

            final SharedPreferences prefs = getActivity().getSharedPreferences(PREFERENCES_LANCHER, Context.MODE_PRIVATE);
            String afterStart = "";
            afterStart = prefs.getString(PREFS_KEY_LAUNCH_AFTER_START, afterStart);
            boolean isAfterStart = false;
            if(item.name.compareTo(afterStart) == 0)
                isAfterStart = true;

            View v;
            TextView tv;
            ImageView iv;

            tv = (TextView) convertView.findViewById(R.id.num);
            tv.setText(String.format("%02d", position + 1));
            final int col1 = 0xffd0d0d0;
            final int col2 = 0xff606060;
            boolean switcher = position % 2 == 0;
            if(isAfterStart)
                tv.setBackgroundColor(0xffff0000);
            else
                tv.setBackgroundColor(switcher ? col1 : col2);
            tv.setTextColor(switcher ? col2 : col1);

            tv = (TextView) convertView.findViewById(R.id.label);
            tv.setSelected(true);
            String splits[] = item.name.split("\\.");
            String label = "";
            if(splits.length > 0)
                label = splits[splits.length-1];
            tv.setText(label);
            tv.setBackgroundColor(switcher ? col1 : col2);
            tv.setTextColor(switcher ? col2 : col1);

            /*tv = (TextView) convertView.findViewById(R.id.category);
            tv.setBackgroundColor(switcher ? col1 : col2);
            Bundle data = item.metaData;
            if(data == null) {
                tv.setText("");
            }
            else {
                tv.setText(data.getInt("category"));
            }*/

            tv = (TextView) convertView.findViewById(R.id.description);
            tv.setSelected(true);
            tv.setText(item.loadLabel(mPackageManager));

            tv = (TextView) convertView.findViewById(R.id.text);
            tv.setSelected(true);
            tv.setText(item.name);
            tv.setBackgroundColor(switcher ? col1 : col2);
            tv.setTextColor(switcher ? col2 : col1);

            iv = (ImageView) convertView.findViewById(R.id.stripe);
            iv.setBackgroundColor(switcher ? col1 : col2);

            return convertView;
        }
    }
}
