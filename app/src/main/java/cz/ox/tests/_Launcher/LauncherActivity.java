package cz.ox.tests._Launcher;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import cz.ox.tests.R;
import cz.ox.tests._Launcher.Fragments.ActivitiesFragment;
import cz.ox.tests._Launcher.Fragments.BaseFragment;
import cz.ox.tests._Launcher.Fragments.CategoriesFragment;

public class LauncherActivity extends ActionBarActivity implements LauncherListener {

    static final String TAG = LauncherActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("onCreate()", "Start");

        setContentView(R.layout.activity_launcher);

        getSupportActionBar().hide();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                            //.add(R.id.container, new MainFragment())
                    .add(R.id.container, new CategoriesFragment(), CategoriesFragment.class.getSimpleName())
                    .commit();


            final SharedPreferences prefs = getSharedPreferences(ActivitiesFragment.PREFERENCES_LANCHER, MODE_PRIVATE);
            String startAfterLaunch = "";
            startAfterLaunch = prefs.getString(ActivitiesFragment.PREFS_KEY_LAUNCH_AFTER_START, startAfterLaunch);
            if(startAfterLaunch != null && startAfterLaunch.length() > 0) {
                Intent intent = new Intent();
                intent.setClassName(this, startAfterLaunch);
                startActivity(intent);
                overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            }
        }
    }

    private int getRandomColor() {
        final double range = 255;
        int r = (int) (Math.random() * range);
        int g = (int) (Math.random() * range);
        int b = (int) (Math.random() * range);
        return Color.rgb(r, g, b);
    }

    @Override
    public void replaceFragment(BaseFragment frag, Bundle data, boolean addToBackStack) {
        try {
            if(data != null)
                frag.setArguments(data);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            /*ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);*/

            ft.replace(R.id.container, frag);
            //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if(addToBackStack)
                ft.addToBackStack(null);
            ft.commit();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
