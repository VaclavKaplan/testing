package cz.ox.tests._Launcher.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import cz.ox.tests._Launcher.LauncherListener;


/**
 * Created by Crusty on 9.8.2014.
 */
public abstract class BaseFragment extends Fragment {

    private LauncherListener mActivityListener;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mActivityListener = (LauncherListener) getActivity();
        }
        catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    protected void showKeyboard(View v) {
        if(v == null)
            return;
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
        //imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
        //imm.showSoftInput(v, 0);
    }

    protected void hideKeyboard(View v) {
        if(v == null)
            return;
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    protected ActionBarActivity getActionBarActivity() {
        return (ActionBarActivity) getActivity();
    }

    protected void replaceFragment(final BaseFragment frag, final Bundle data, final boolean addToBackStack) {
        mActivityListener.replaceFragment(frag, data, addToBackStack);
    }
}
