package cz.ox.tests._Launcher.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Locale;

import cz.ox.tests.R;

public class CategoriesFragment extends BaseFragment {

    public static final int CATEGORY_UNDEFINED = -1;

    private ViewGroup mRootView;

    final ArrayList<Category> mCategories = new ArrayList<>();

    public CategoriesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initCategories();
    }

    private void initCategories() {
        ArrayList<ActivityInfo> activities = null;
        try {
            PackageInfo pi = getActivity().getPackageManager()
                    .getPackageInfo("cz.ox.tests",
                            PackageManager.GET_ACTIVITIES | PackageManager.GET_META_DATA);
            if(pi == null)
                return;
            activities = new ArrayList<ActivityInfo>(Arrays.asList(pi.activities));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(activities == null)
            return;

        HashMap<Integer, Integer> counters = new HashMap<Integer, Integer>();
        counters.put(CATEGORY_UNDEFINED, 0);

        HashSet<Integer> categories = new HashSet<Integer>();

        // get after launch activity
        final SharedPreferences prefs = getActivity().getSharedPreferences(ActivitiesFragment.PREFERENCES_LANCHER, Context.MODE_PRIVATE);
        String afterLaunch = "";
        afterLaunch = prefs.getString(ActivitiesFragment.PREFS_KEY_LAUNCH_AFTER_START, afterLaunch);
        int afterLaunchCat = 0;

        // get only by category
        // remove splash, launcher activities
        ActivityInfo ai;
        ListIterator li = activities.listIterator();
        int cat;
        while(li.hasNext()) {
            ai = (ActivityInfo) li.next();

            if(ai.metaData != null) {
                cat = ai.metaData.getInt("category");

                if(ai.name.compareTo(afterLaunch) == 0) {
                    afterLaunchCat = cat;
                }

                if(cat != 0) {
                    if (counters.containsKey(cat))
                        counters.put(cat, counters.get(cat)+1);
                    else
                        counters.put(cat, 1);

                    categories.add(cat);
                    continue;
                }
            }

            if(ai.name.contains("LauncherActivity")) {
                li.remove();
                continue;
            }

            if(ai.name.contains("SplashActivity")) {
                li.remove();
                continue;
            }

            // if we get here its activity without defined category
            counters.put(CATEGORY_UNDEFINED, counters.get(CATEGORY_UNDEFINED)+1);
        }

        // finaly init categories
        int count;
        boolean b;
        for(int c : categories) {
            count = counters.get(c);
            b = c == afterLaunchCat;
            mCategories.add(new Category(c, count, b));
        }

        // add undefined
        mCategories.add(new Category(CATEGORY_UNDEFINED, counters.get(CATEGORY_UNDEFINED), false));

        // sort
        Collections.sort(mCategories, new Comparator<Category>() {
            @Override
            public int compare(Category lhs, Category rhs) {
                Collator usCollator = Collator.getInstance(Locale.getDefault());
                usCollator.setStrength(Collator.PRIMARY);
                return usCollator.compare(lhs.getNameString(), rhs.getNameString());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //inflater.inflate(R.menu., menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v(CategoriesFragment.class.getSimpleName(), "onCreateView");

        final Context context = getActivity();

        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_launcher_categories, container, false);

        ImageView iv;

        CategoriesAdapter adapter = new CategoriesAdapter(getActivity());
        adapter.addAll(mCategories);

        GridView grid = (GridView) mRootView.findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category category = (Category) parent.getItemAtPosition(position);
                Bundle data = new Bundle();
                data.putInt("category", category.mNameRes);
                replaceFragment(new ActivitiesFragment(), data, true);
            }
        });

        TextView tv;

        tv = (TextView) mRootView.findViewById(R.id.footer);
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0);

            tv.setText("v "+pInfo.versionName+" ("+pInfo.versionCode+")");
        }
        catch (PackageManager.NameNotFoundException e) {
            tv.setText("www.2ox.cz");
        }

        final View helpView = inflater.inflate(R.layout.launcher_category_help, null);
        ImageView logo = (ImageView) helpView.findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim = AnimationUtils.loadAnimation(v.getContext(),
                        R.anim.bounce);
                v.startAnimation(anim);
            }
        });


        iv = (ImageView) mRootView.findViewById(R.id.help);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder
                        .setIcon(R.drawable.ic_launcher)
                        .setTitle("VO CO GO")
                        .setView(helpView)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                ;
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        updateAfterLaunchMark();
    }

    private void updateAfterLaunchMark() {
        // get after launch activity
        final SharedPreferences prefs = getActivity().getSharedPreferences(ActivitiesFragment.PREFERENCES_LANCHER, Context.MODE_PRIVATE);
        String afterLaunch = "";
        afterLaunch = prefs.getString(ActivitiesFragment.PREFS_KEY_LAUNCH_AFTER_START, afterLaunch);
        if(afterLaunch.length() == 0)
            return;

        for(Category cat : mCategories) {
                        
        }
    }

    public class Category {
        public int mNameRes;
        public int mNumActivities;
        public boolean mContainAfterLaunch;

        public Category(int name, int numActs, boolean containAfterLaunch) {
            mNameRes = name;
            mNumActivities = numActs;
            mContainAfterLaunch = containAfterLaunch;
        }

        @Override
        public String toString() {
            return "Category "+mNameRes;
        }

        public String getNameString() {
            if(mNameRes == CATEGORY_UNDEFINED)
                return getString(R.string.category_undefined);

            return getString(mNameRes);
        }
    }

    public class CategoriesAdapter extends ArrayAdapter<Category> {

        private final static int ITEM_LAYOUT = R.layout.launcher_item_category;

        private final Context context;

        public CategoriesAdapter(Context context) {
            super(context, ITEM_LAYOUT);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(convertView == null)
                convertView = inflater.inflate(ITEM_LAYOUT, parent, false);

            Category item = getItem(position);

            TextView textView;

            textView = (TextView) convertView.findViewById(R.id.category);
            if(item.mNameRes == CATEGORY_UNDEFINED)
                textView.setText(R.string.category_undefined);
            else
                textView.setText(item.mNameRes);

            textView = (TextView) convertView.findViewById(R.id.num);
            if(item.mContainAfterLaunch)
                textView.setTextColor(0xffff0000);
            else
                textView.setTextColor(0xffffffff);
            textView.setText(item.mNumActivities+"x");

            return convertView;
        }
    }


    private class LongOperation extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Integer... par) {
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {}

        @Override
        protected void onPostExecute(Void v) {
            Log.d("", "@@@ done");
            if(isAdded() == false)
                return;
        }
    }
}
