package cz.ox.tests._Launcher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

//import com.crashlytics.android.Crashlytics;

import cz.ox.tests.R;

public class SplashActivity extends Activity {

    private ImageView mLogo;
    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Crashlytics.start(this);

        setContentView(R.layout.activity_splash);

        mLogo = (ImageView) findViewById(R.id.logo);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mHandler.postDelayed(mRunnable, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mLogo.setAnimation(anim);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                goToLauncherActivity();
            }
        };

        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLogo.getAnimation().start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mRunnable);
    }

    private void goToLauncherActivity() {
        Intent intent = new Intent(this, LauncherActivity.class);
        //intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

        finish();
    }
}
