package cz.ox.tests._Launcher;

import android.os.Bundle;

import cz.ox.tests._Launcher.Fragments.BaseFragment;

/**
 * Created by Crusty on 6.7.2015.
 */
public interface LauncherListener {
    void replaceFragment(BaseFragment frag, Bundle data, boolean addToBackStack);
}
