package cz.ox.tests.Math;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import cz.ox.tests.R;

/**
 * Created by Crusty on 02.11.2016.
 */
public class BinaryActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_binary);

        final TextView tv = (TextView) findViewById(R.id.text);

        EditText et = (EditText) findViewById(R.id.edit);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                int num = 0;
                try {
                    num = Integer.parseInt(s.toString());
                }
                catch (NumberFormatException ex) {
                }

                updateText(tv, num);
                tv.append("\n");
                operators(tv, num);
            }
        });

        int a = 0b10101;
        operators(tv, a);

    }

    private void updateText(TextView tv, int num) {
        tv.setText("Num = "+num);
        tv.append("\n");
        tv.append(Integer.toBinaryString(num));
        tv.append("\n");
        tv.append(Integer.toOctalString(num));
        tv.append("\n");
        tv.append(Integer.toHexString(num));
        tv.append("\n\n");
    }

    private void line(TextView tv, String operator, int result) {
        tv.append(operator);
        tv.append(" ");
        tv.append(Integer.toBinaryString(result));
        tv.append("\n");
    }

    private void operators(TextView tv, int a) {
        tv.append(Integer.toBinaryString(a));
        tv.append("\n\n");

        line(tv, "`\t", ~a);
        line(tv, ">> 1\t", a >> 1);
        line(tv, ">>> 1\t", a >>> 1);
        line(tv, "<< 1\t", a << 1);
        line(tv, "& 1\t", a & 1);
        line(tv, "| 1\t", a | 1);
        line(tv, "^ 1\t", a ^ 1);
        tv.append("\n");
        line(tv, "& 0b11111\t", a & 0b11111);
        line(tv, "| 0b11111\t", a | 0b11111);
        line(tv, "^ 0b11111\t", a ^ 0b11111);
    }
}
