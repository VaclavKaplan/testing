package cz.ox.tests.test5;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cz.ox.tests.R;

/**
 * Created by Crusty on 25.9.2014.
 */
public class Test5Activity extends Activity {

    static final int DELAY = 150;

    int mEndIndex = 1;

    MediaPlayer mMediaPlayer = null;

    Runnable mTextRunnable = null;

    public TextView tvCislo;
    public Odpocet casovac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test5);

        final TextView text = (TextView) findViewById(R.id.text);

        Button b;
        b = (Button) findViewById(R.id.btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.removeCallbacks(mTextRunnable);
                mEndIndex = 0;
                text.postDelayed(mTextRunnable, DELAY);
            }
        });

        /*QuizDefinitions.Quiz quiz;
        final StringBuilder sb = new StringBuilder();
        sb.append("\n");
        quiz = QuizDefinitions.getQuiz(0);
        sb.append(quiz.toString() + "\n");
        quiz = QuizDefinitions.getQuiz(1);
        sb.append(quiz.toString() + "\n");
        quiz = QuizDefinitions.getQuiz(2);
        sb.append(quiz.toString() + "\n");*/

        final StringBuilder sb = new StringBuilder();
        sb.append("Na smetišti leží tyčka,\n" +
                "na konci je ostrá špička\n" +
                "okousaná od zajíčka.\n" +
                "Napíchl tam Netolička\n" +
                "gumovýho andělíčka,\n" +
                "červenají se mu líčka.\n" +
                "V dálce křičí koroptvička...");

        final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        mTextRunnable = new Runnable() {
            @Override
            public void run() {
                final String s = sb.substring(0, mEndIndex);
                text.setText(s);

                //tg.startTone(ToneGenerator.TONE_PROP_PROMPT);

                if (mEndIndex < sb.length()) {
                    mEndIndex++;
                    text.postDelayed(this, DELAY);
                }
            }
        };
        text.postDelayed(mTextRunnable, DELAY);

        // text rotation

        tvCislo = (TextView) findViewById(R.id.text2);
        Zapni();
    }

    class Barva {
        String mText;
        int mBarva;

        Barva(String text, int barva) {
            mText = text;
            mBarva = barva;
        }
    }
    public void Zapni ()
    {
        Barva barvy[] = {
                new Barva("cervena", 0xffff0000),
                new Barva("zelena", 0xff00ff00),
                new Barva("modra", 0xff0000ff),
        };
        int delkaPauzy = 2000;

        casovac =  new Odpocet(delkaPauzy, delkaPauzy, barvy);
        casovac.start();
    }

    public class Odpocet extends CountDownTimer {

        final Barva mBarvy[];
        int aktualniBarva = 0; // nastaveno na prvni barvu

        public Odpocet(long millisInFuture, long countDownInterval, Barva[] barvy) {
            super(millisInFuture, countDownInterval);
            mBarvy = barvy;

            // nastavime hned prvni barvu
            tvCislo.setText(barvy[aktualniBarva].mText);
            tvCislo.setBackgroundColor(barvy[aktualniBarva].mBarva);
        }

        @Override
        public void onFinish() {
            // nejdriv upravit aktualniBarvu, prvni nastavCernouBarvu uz byla
            aktualniBarva++;
            if(aktualniBarva >= mBarvy.length)
                aktualniBarva = 0;

            tvCislo.setText(mBarvy[aktualniBarva].mText);
            tvCislo.setBackgroundColor(mBarvy[aktualniBarva].mBarva);

            // a znova spustit pro dalsi barvu
            casovac.start();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}








