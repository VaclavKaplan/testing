package cz.ox.tests.test5;

import java.util.ArrayList;

import cz.ox.tests.R;

/**
 * Created by Crusty on 5.7.2015.
 */
public class QuizDefinitions {

    public static class Quiz {
        public int mId;             // muze byt cislo kvizu
        public int mObrazekRes;     // R.drawable. ID obrazku
        public int mOtazkaRes;      // R.string. ID textu, otazky kvizu

        public Quiz(int id, int obrazekRes, int otazkaRes) {
            mId = id;
            mObrazekRes = obrazekRes;
            mOtazkaRes = otazkaRes;
        }

        @Override
        public String toString() {
            return "ID "+mId+" Obr "+mObrazekRes+" Otaz "+mOtazkaRes;
        }
    }

    /** tady jsou ulozeny vsechny otazky */
    private static final ArrayList<Quiz> mQuizzes = new ArrayList<Quiz>();

    static {
        init();
    }

    private static void init() {
        // tady si vytvoris otazky
        /*mQuizzes.add(new Quiz(1, R.drawable.icon_cislo_01, R.string.otazka_01));
        mQuizzes.add(new Quiz(2, R.drawable.icon_cislo_02, R.string.otazka_02));
        mQuizzes.add(new Quiz(3, R.drawable.icon_cislo_03, R.string.otazka_03));*/

        mQuizzes.add(new Quiz(1, 11, 111));
        mQuizzes.add(new Quiz(2, 22, 222));
        mQuizzes.add(new Quiz(3, 33, 333));
    }

    // vrati konkretni otazku
    public static Quiz getQuiz(int index) {
        return mQuizzes.get(index); // pro ukazku vraci dle indexu (od nuly, ..), jinak muze hledat dle ID, ...
    }
}
